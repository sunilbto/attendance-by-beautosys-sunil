package com.beautosys.attendance.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@ToString
@Entity
public class Employee {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;
    
    @Column(nullable=false)
    @NotNull
    private String name;
    
    @Column(nullable=false)
    @NotNull
    private LocalDateTime joiningDate;
    private boolean status;
    public Employee(String name, LocalDateTime joiningDate, boolean status) {
        this.name = name;
        this.joiningDate = joiningDate;
        this.status = status;
    }
    
    
}
