package com.beautosys.attendance.model;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@ToString
@Entity
public class Attendance {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private long employeeId;
    private LocalDateTime dateOfAttendance;
    private LocalDateTime checkInTime;
    private LocalDateTime lunchTimeOut;
    private LocalDateTime lunchTimeIn;
    private LocalDateTime checkOutTime;
    private boolean status;

    public Attendance(long employeeId, LocalDateTime dateOfAttendance, LocalDateTime checkInTime,
            LocalDateTime lunchTimeOut, LocalDateTime lunchTimeIn, LocalDateTime checkOutTime, boolean status) {
        this.employeeId = employeeId;
        this.dateOfAttendance = dateOfAttendance;
        this.checkInTime = checkInTime;
        this.lunchTimeOut = lunchTimeOut;
        this.lunchTimeIn = lunchTimeIn;
        this.checkOutTime = checkOutTime;
        this.status = status;
    }

}
