package com.beautosys.attendance.repository;

import org.springframework.data.repository.CrudRepository;

import com.beautosys.attendance.model.Attendance;

public interface AttendanceRepository extends CrudRepository<Attendance, Long> {

}
