package com.beautosys.attendance.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.beautosys.attendance.model.Employee;

public interface EmployeeRepository extends CrudRepository<Employee, Long> {

    Optional<Employee> findByName(String name);

    Iterable<Employee> findAllByName(String name);

}
