package com.beautosys.attendance.service;

import java.io.StringReader;
import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.beautosys.attendance.model.Attendance;
import com.beautosys.attendance.repository.AttendanceRepository;
import com.opencsv.CSVReader;

@Service
public class ImportAttendanceServiceImpl implements ImportAttendanceService {

    @Autowired
    AttendanceRepository attendanceRepo;

    @Override
    public ResponseEntity saveImportedAttendance(MultipartFile file) {
        if (file.isEmpty()) {
            return new ResponseEntity("please select a file!", HttpStatus.LENGTH_REQUIRED);
        }
        try {
            byte[] bytes = file.getBytes();
            String fileFrom = new String(bytes);
            try (CSVReader csvReader = new CSVReader(new StringReader(fileFrom))) {
                String[] values = null;
                boolean shouldAdd = Boolean.FALSE;
                while ((values = csvReader.readNext()) != null) {
                    Attendance attendance = null;
                    if (values.length == 8 && shouldAdd) {
                        attendance = new Attendance();

                        try {
                            attendance.setEmployeeId(Long.parseLong(values[1].replace(" ", "")));
                            attendance.setDateOfAttendance(LocalDateTime.parse(values[2].replace(" ", "")));
                            attendance.setCheckInTime(LocalDateTime.parse(values[3].replace(" ", "")));
                            attendance.setLunchTimeOut(LocalDateTime.parse(values[4].replace(" ", "")));
                            attendance.setLunchTimeIn(LocalDateTime.parse(values[5].replace(" ", "")));
                            attendance.setCheckOutTime(LocalDateTime.parse(values[6].replace(" ", "")));
                            if (values[7].equals("1")) {
                                attendance.setStatus(Boolean.TRUE);
                            } else {
                                attendance.setStatus(Boolean.FALSE);
                            }
                        } catch (Exception e) {
                            attendance = null;
                            System.out
                                    .println("Exception while Object creation" + LocalDateTime.now() + e.getMessage());
                            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
                        }

                    }
                    if (attendance != null) {
                        try {
                            attendance = attendanceRepo.save(attendance);
                        } catch (Exception e) {
                            System.out.println("Exception while Persisting Attendace" + e.getMessage());
                            e.printStackTrace();
                        }
                    }
                    shouldAdd = Boolean.TRUE;
                }
            }
        } catch (Exception e) {
            System.out.println("Exception while file creation" + e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity("Successfully Imported To Db - " + file.getOriginalFilename(), new HttpHeaders(),
                HttpStatus.OK);
    }

}
