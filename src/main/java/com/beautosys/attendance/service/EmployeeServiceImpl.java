package com.beautosys.attendance.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.beautosys.attendance.exceptions.EmployeeNotFoundException;
import com.beautosys.attendance.exceptions.NoContentException;
import com.beautosys.attendance.model.Employee;
import com.beautosys.attendance.repository.EmployeeRepository;

@Service
public class EmployeeServiceImpl implements EmployeeService{

    @Autowired
    EmployeeRepository emplRepo;
    
    @Override
    public Employee saveEmployee(Employee employee) {
        if(employee!=null) {
            try {
                employee = emplRepo.save(employee);
            } catch (Exception e) {
                System.out.println("Exception While Saving Employee : "+e.getMessage());
            }
        }
        return employee;
    }

    @Override
    public List<Employee> getAllEmployees() {
        List<Employee> employeeList = new ArrayList<Employee>();
        try {
            emplRepo.findAll().forEach(employeeList::add);
        } catch (Exception e) {
            System.out.println("Exception while fetching employees : "+e.getMessage());
        }
        if(employeeList.size()<1) {
            throw new NoContentException();
        }
        return employeeList;
    }

    @Override
    public List<Employee> findEmployeeByName(String name) {
        List<Employee> employeeList = new ArrayList<Employee>();
        try {
            emplRepo.findAllByName(name).forEach(employeeList::add);
        } catch (Exception e) {
            System.out.println("Exception While finding employee : "+e.getMessage());
            throw new EmployeeNotFoundException();
        }
        return employeeList;
    }

    @Override
    public Employee getEmployeeDetailsById(long id) {
        Employee employee = null;
        try {
            employee = emplRepo.findById(id).get();
        } catch (Exception e) {
            System.out.println("Exception : EmployeeNotFoundException "+e.getMessage());
            throw new EmployeeNotFoundException();
        }
        return employee;
    }

    @Override
    public Employee updateEmployee(Employee employee) {
        Employee employeeFromDb = null;
        try {
            employeeFromDb = emplRepo.findById(employee.getId()).get();
        } catch (Exception e) {
            System.out.println("Employee not found Exception : "+e.getMessage());
            throw new EmployeeNotFoundException();
        }
        try {
            employeeFromDb.setName(employee.getName());
            employeeFromDb.setJoiningDate(employee.getJoiningDate());
            employeeFromDb.setStatus(employee.isStatus());
            employeeFromDb = emplRepo.save(employeeFromDb);
        } catch (Exception e) {
            System.out.println("Exception while updating employee : "+e.getMessage());
        }
        return employeeFromDb;
    }

    @Override
    public void deleteEmployee(long id) {
        Employee employeeFromDb = null;
        try {
            employeeFromDb = emplRepo.findById(id).get();
        } catch (Exception e) {
            System.out.println("Employee not found Exception : "+e.getMessage());
            throw new EmployeeNotFoundException();
        }
        if(employeeFromDb!=null) {
            emplRepo.delete(employeeFromDb);
        }
    }

}
