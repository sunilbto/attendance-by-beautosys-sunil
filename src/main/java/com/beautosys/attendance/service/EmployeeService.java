package com.beautosys.attendance.service;

import java.util.List;

import com.beautosys.attendance.model.Employee;

public interface EmployeeService {
    Employee saveEmployee(Employee employee);

    List<Employee> getAllEmployees();

    List<Employee> findEmployeeByName(String name);

    Employee getEmployeeDetailsById(long id);

    Employee updateEmployee(Employee employee);

    void deleteEmployee(long id);
}
