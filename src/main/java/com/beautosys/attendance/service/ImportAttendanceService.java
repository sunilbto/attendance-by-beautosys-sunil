package com.beautosys.attendance.service;

import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

public interface ImportAttendanceService {

    ResponseEntity saveImportedAttendance(MultipartFile file);

}