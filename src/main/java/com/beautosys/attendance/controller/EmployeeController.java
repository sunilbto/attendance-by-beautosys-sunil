package com.beautosys.attendance.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.beautosys.attendance.model.Employee;
import com.beautosys.attendance.service.EmployeeService;

@RestController
@RequestMapping(value = "/api/employee", produces = "application/json")
public class EmployeeController {

    @Autowired
    EmployeeService emplService;

    @RequestMapping(method = RequestMethod.POST, consumes = "application/json")
    public Employee createEmployee(@RequestBody @Valid Employee employee) {
        employee = emplService.saveEmployee(employee);
        return employee;
    }

    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public List<Employee> getAllEmployee() {
        List<Employee> employeesFromDb = null;
        employeesFromDb = emplService.getAllEmployees();
        return employeesFromDb;
    }

    @RequestMapping(method = RequestMethod.GET, produces = "application/json", value = "{id}")
    public Employee getEmployeeDetails(@PathVariable long id) {
        Employee employeeFromDb = null;
        employeeFromDb = emplService.getEmployeeDetailsById(id);
        return employeeFromDb;
    }

    @RequestMapping(method = RequestMethod.PUT, consumes = "application/json", value = "{id}")
    public Employee updateTask(@RequestBody @Valid Employee employee) {
        employee = emplService.updateEmployee(employee);
        return employee;
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "{id}")
    public ResponseEntity<?> deleteTask(@PathVariable long id) {
        emplService.deleteEmployee(id);
        return new ResponseEntity("Employee Deleted Successfully!", new HttpHeaders(), HttpStatus.OK);
    }

}
