package com.beautosys.attendance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.beautosys.attendance.service.ImportAttendanceService;

@RestController
public class ImportAttendanceController {

     @Autowired
     ImportAttendanceService importAttendanceService;

     @PostMapping("/api/upload")
     public ResponseEntity<?> uploadFile(@RequestParam("csvFile") MultipartFile uploadfile) {
          return importAttendanceService.saveImportedAttendance(uploadfile);
     }

}
