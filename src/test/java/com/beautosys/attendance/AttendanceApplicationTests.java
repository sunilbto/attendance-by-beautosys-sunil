package com.beautosys.attendance;

import java.io.File;
import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.HttpStatus;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import com.beautosys.attendance.model.Employee;
import com.beautosys.attendance.repository.EmployeeRepository;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class AttendanceApplicationTests {

    @Value("${local.server.port}")
    protected int serverPort;

    @Autowired
    EmployeeRepository empRepo;

    @Before
    public void setUp() {
        RestAssured.port = serverPort;
    }
    
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
    @Test
    public void UploadCsvAndSaveToDbSuccessTest() {
        ClassLoader classLoader = getClass().getClassLoader();
        File csvFile = new File(classLoader.getResource("csv_file.csv").getFile());
        Response response = given().multiPart("csvFile", csvFile, "application/octet-stream").post("/api/upload");
        assertEquals(HttpStatus.OK.value(), response.statusCode());
    }

    @Test
    public void createEmployeeSuccessTest() {
        String name = "Happy";
        LocalDateTime joiningDate = LocalDateTime.now();
        boolean status = Boolean.TRUE;

        Employee employee = new Employee(name, joiningDate, status);
        given().body(employee).contentType(ContentType.JSON).when().post("/api/employee").then()
                .statusCode(equalTo(HttpStatus.OK.value())).body("id", not(nullValue())).body("name", is(name));
        Optional<Employee> employeeOnRecord = empRepo.findByName(name);
        assertEquals(name, employeeOnRecord.get().getName());
    }

    @Test
    public void createEmployeeWithNullValuesFailTest() {
        String name = null;
        LocalDateTime joiningDate = LocalDateTime.now();
        boolean status = Boolean.TRUE;

        Employee employee = new Employee(name, joiningDate, status);
        given().body(employee).contentType(ContentType.JSON).when().post("/api/employee").then()
                .statusCode(equalTo(HttpStatus.BAD_REQUEST.value()));
    }

    @Test
    public void updateEmployeeWithWrongEmployeeIdFailTest() {
        String name = "Vikas";
        LocalDateTime joiningDate = LocalDateTime.now();
        boolean status = Boolean.TRUE;
        Employee employee = new Employee(name, joiningDate, status);
        given().body(employee).contentType(ContentType.JSON).when().pathParam("empId", employee.getId())
                .put("/api/employee/{empId}").then().statusCode(equalTo(HttpStatus.NOT_FOUND.value()));
    }

    @Test
    public void updateEmployeePassTest() {
        String name = "Vikas";
        String newName = "Laxman";
        LocalDateTime joiningDate = LocalDateTime.now();
        boolean status = Boolean.TRUE;
        Employee employee = new Employee(name, joiningDate, status);
        employee = empRepo.save(employee);
        employee.setName(newName);
        given().body(employee).contentType(ContentType.JSON).when().pathParam("empId", employee.getId())
                .put("/api/employee/{empId}").then().statusCode(equalTo(HttpStatus.OK.value()))
                .body("name", is(newName));
    }

    @DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
    @Test
    public void getAllEmployeeSuccessTest() {
        String name = "Akash";
        LocalDateTime joiningDate = LocalDateTime.now();
        boolean status = Boolean.TRUE;
        Employee employee = new Employee(name, joiningDate, status);
        String name2 = "Dipak";
        LocalDateTime joiningDate2 = LocalDateTime.now();
        boolean status2 = Boolean.TRUE;
        Employee employee2 = new Employee(name2, joiningDate2, status2);
        empRepo.save(employee);
        empRepo.save(employee2);
        Response resp = given().get("/api/employee");
        resp.then().statusCode(equalTo(HttpStatus.OK.value())).body("size()", is(2));
    }

    @DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
    @Test
    public void getAllEmployeeWithNoRecordsFailTest() {
        Response resp = given().get("/api/employee");
        resp.then().statusCode(equalTo(HttpStatus.NO_CONTENT.value()));
    }

    @Test
    public void getEmployeeDetailsSuccessTest() {
        String name = "Akash";
        LocalDateTime joiningDate = LocalDateTime.now();
        boolean status = Boolean.TRUE;
        Employee employee = new Employee(name, joiningDate, status);
        employee = empRepo.save(employee);
        Response resp = given().pathParam("empId", employee.getId()).get("/api/employee/{empId}");
        resp.then().statusCode(equalTo(HttpStatus.OK.value())).body("name", is(name));
    }

    @Test
    public void getEmployeeDetailsWithWrongEmployeeIdFailTest() {
        Response resp = given().pathParam("empId", 0).get("/api/employee/{empId}");
        resp.then().statusCode(equalTo(HttpStatus.NOT_FOUND.value()));
    }

    @Test
    public void deleteEmployeeSuccessTest() {
        String name = "Akash";
        LocalDateTime joiningDate = LocalDateTime.now();
        boolean status = Boolean.TRUE;
        Employee employee = new Employee(name, joiningDate, status);
        employee = empRepo.save(employee);
        given().pathParam("empid", employee.getId()).delete("/api/employee/{empid}").then()
                .statusCode(equalTo(HttpStatus.OK.value()));
        assertFalse(empRepo.findById(employee.getId()).isPresent());
    }

    @Test
    public void deleteEmployeeWithWrongEmployeeIdFailTest() {
        given().pathParam("empid", 0).delete("/api/employee/{empid}").then()
                .statusCode(equalTo(HttpStatus.NOT_FOUND.value()));
    }
}